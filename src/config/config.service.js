/* eslint-disable */
// Incluye cada servicio junto a su configuración

export default {
  hdfs: {
    url: "/modelo-dwp/providers/hdfs.php",
    mockUrl: "http://localhost/modelo-dwp/providers/hdfs.php",
    method: 'get',
    mock: true
  },
  datamodelalpha: {
    url: "/modelo-dwp/providers/datamodelalpha.php",
    mockUrl: "http://localhost/modelo-dwp/providers/datamodelalpha.php?solution1={solution1}&alpha2={alpha2}&alpha3={alpha3}&alpha4={alpha4}",
    method: 'get',
    mock: true
  },
  datamodelreferences: {
    url: "/modelo-dwp/providers/datamodelreferences.php",
    mockUrl: "http://localhost/modelo-dwp/providers/datamodelreferences.php",
    method: 'get',
    mock: true
  },
  datamodelproyect: {
    url: "/modelo-dwp/providers/datamodelproyect.php?numProyect=",
    mockUrl: "http://localhost/modelo-dwp/providers/datamodelproyect.php?numProyect=",
    method: 'get',
    mock: true
  },
  hdfsproyect: {
    url: "/modelo-dwp/providers/hdfsproyect.php?numProyect=",
    mockUrl: "http://localhost/modelo-dwp/providers/hdfsproyect.php?numProyect=",
    method: 'get',
    mock: true
  }
};
