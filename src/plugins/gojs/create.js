/* eslint-disable */
//import clone from './clone.js';
import ctors from './constructors.js';

function render (createElement) {
  return createElement('div')
}

function create (tagName, Gojs, Vue) {
  var $ = Gojs.GraphObject.make
  var Ctor = Gojs[ctors[tagName]]
  if (!Ctor) {
    return { render: render } // Gojs.win
    // ? null
    // When running in server, GoJS will not be instanced,
    // so there're no constructors in GoJS,
    // to avoid unmated content during SSR, it returns minimum component.
    // : { render: render }
  }
  var isRenderer = tagName === 'gojs-renderer'
  var component = {
    name: tagName,
    data () {
      return {
        myDiagram: {}
      }
    },
    props: {
      options: { type: Object, required: true },
      modelData: { type: Object, required: true }
    },
    methods: {
      _initDiagram: function () {
        this._renderDiagram()
        this.$watch('options', this._renderDiagram, { deep: true })
        this.$watch('modelData', this._renderDiagram, { deep: true })
      },
      _renderDiagram: function () {
        let self = this
        if (!isRenderer) {
          if (this.$el.innerHTML !== '') this.diagram.div = null // https://forum.nwoods.com/t/trying-to-reload-the-diagram/8476
          this.myDiagram = $(Ctor, this.$el, // create a Diagram for the DIV HTML element
            {
              initialContentAlignment: Gojs.Spot.Center,
              initialAutoScale: Gojs.Diagram.Uniform,
              //layout: $(Gojs.TreeLayout, { angle: 90, arrangement: Gojs.TreeLayout.ArrangementHorizontal }),
              allowDelete: false,
              allowCopy: false,
              layout: $(Gojs.TreeLayout,
                { comparer: Gojs.LayoutVertex.smartComparer }),
              // enable undo & redo
              'undoManager.isEnabled': true,
              // Model ChangedEvents get passed up to component users
              'ModelChanged': function (e) { self.$emit('model-changed', e) },
              'ChangedSelection': function (e) { self.$emit('changed-selection', e) }
            }) // new Ctor(this.$el, this.width, this.height)
          // this.diagram = myDiagram
        } else {
          this.myDiagram = $(Ctor, this.$el, // create a Diagram for the DIV HTML element
            {
              // enable undo & redo
              initialAutoScale: Gojs.Diagram.Uniform,
              initialContentAlignment: Gojs.Spot.Center,
              allowDelete: false,
              allowCopy: false,
              layout: $(Gojs.TreeLayout,
                  { comparer: Gojs.LayoutVertex.smartComparer }),
              'undoManager.isEnabled': true
            }) // new Ctor(this.$el, clone(this.options)) 
        }
        // this.diagram = myDiagram
        this.createNode()
        this.diagram = this.myDiagram
        this.updateModel(this.modelData)
      },
      createNode: function () { // Esto deberemos añadirlo en funcion de los datos que vengan en options
        /*this.myDiagram.nodeTemplate = $(Gojs.Node,
          'Auto',
          $(Gojs.Shape,
            {
              fill: 'white',
              strokeWidth: 0,
              portId: '',
              fromLinkable: true,
              toLinkable: true,
              cursor: 'pointer'
            },
            new Gojs.Binding('fill', 'color')),
          $(Gojs.TextBlock,
            { margin: 8, editable: true },
            new Gojs.Binding('text').makeTwoWay())
        )*/
        this.myDiagram.linkTemplate =
        $(Gojs.Link, // the whole link panel
            {
                selectionAdorned: true,
                layerName: 'Foreground',
                reshapable: true,
                routing: Gojs.Link.AvoidsNodes,
                corner: 5,
                curve: Gojs.Link.JumpOver
            },
            $(Gojs.Shape, // the link shape
                { stroke: '#072146', strokeWidth: 2.5 }),
            $(Gojs.TextBlock, // the 'from' label
                {
                    textAlign: 'center',
                    font: '14px "Benton Sans Bold"',
                    stroke: '#043263',
                    segmentIndex: 0,
                    segmentOffset: new Gojs.Point(NaN, NaN),
                    segmentOrientation: Gojs.Link.OrientUpright
                },
                new Gojs.Binding('text', 'text')),
            $(Gojs.TextBlock, // the 'to' label
                {
                    textAlign: 'center',
                    font: '14px "Benton Sans Bold"',
                    stroke: '#043263',
                    segmentIndex: -1,
                    segmentOffset: new Gojs.Point(NaN, NaN),
                    segmentOrientation: Gojs.Link.OrientUpright
                },
                new Gojs.Binding('text', 'toText'))
        );
        let simpletemplate =
          $(Gojs.Node, 'Auto',
            { click: this.showDetailDiagram },
            $(Gojs.Panel, 'Auto',
              $(Gojs.Shape, 'Rectangle',
                {
                  stroke: 'transparent', strokeWidth: 0, cursor: 'pointer', background: '#1973b8',
                },
                new Gojs.Binding('fill', 'isHighlighted', function (h) { return h ? '#f35e61' : '#1973b8'; }).ofObject(),
                new Gojs.Binding('fill', 'isSelected', function (sel) {
                  return sel ? '#f35e61' : '#1973b8';
                }).ofObject()),
              $(Gojs.TextBlock,
                {
                  margin: new Gojs.Margin(15, 25, 15, 15),
                  stroke: '#fff',
                  font: '14px "Benton Sans Bold"'
                },
                new Gojs.Binding('text', 'key'))
            )
        );
        this.myDiagram.nodeTemplate = simpletemplate;
        /*this.myDiagram.addDiagramListener('ViewportBoundsChanged',
        function (e) {
            // this.myDiagram.nodeTemplate = detailtemplate;
            //if (this.myDiagram.scale < 0.7) {
              self.myDiagram.nodeTemplate = simpletemplate;
            //} else {
                //this.myDiagram.nodeTemplate = detailtemplate;
            //}
        });*/
        /*$(Gojs.Link,
          { relinkableFrom: true, relinkableTo: true },
          $(Gojs.Shape),
          $(Gojs.Shape, { toArrow: 'OpenTriangle' })
        )*/
      },
      showDetailDiagram: function() {

      },
      updateModel: function (val) {
        // No GoJS transaction permitted when replacing Diagram.model.
        if (val instanceof Gojs.Model) {
          this.diagram.model = val
        } else {
          var m = new Gojs.GraphLinksModel()
          if (val) {
            for (var p in val) {
              m[p] = val[p]
            }
          }
          this.diagram.model = m
        }
      }
    },
    beforeDestroy: function () {
      if (!isRenderer) {
        this.diagram.div = null
        // this.$el.removeChild(this.diagram.box)
        for (var property in this.diagram) {
          delete this.diagram[property]
        }
        this.diagram = null
      } else {
        this.diagram.destroy()
      }
    }
  }
  var isVue1 = /^1\./.test(Vue.version)
  if (isVue1) {
    component.template = '<div></div>'
    component.ready = function () {
      this._initDiagram()
    }
  } else {
    component.render = render
    component.mounted = function () {
      this._initDiagram()
    }
  }
  return component
}

export default create
