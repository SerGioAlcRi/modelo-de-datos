/* eslint-disable */
/* ============
 * Vue i18n
 * ============
 *
 * Internationalization plugin of Vue.js.
 *
 * https://kazupon.github.io/vue-i18n/
 */

import go from 'gojs'
import ctors from './constructors'
import create from './create'

export default {
  install (Vue, options) {
    // const { options } = optionsGo
    var Go = (options && options.go) || go
    Vue.prototype.$Go = Go // .GraphObject.make
    for (var tagName in ctors) {
      var component = create(tagName, Go, Vue)
      component && Vue.component(tagName, component)
    }
  }
}
