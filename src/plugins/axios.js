/* ============
 * Axios
 * ============
 *
 * Promise based HTTP client for the browser and node.js.
 * Because Vue Resource has been retired, Axios will now been used
 * to perform AJAX-requests.
 *
 * https://github.com/mzabriskie/axios
 */

import Vue from 'vue';
import Axios from 'axios';

/*const config = require('../../config');

if (window.location.hostname === 'localhost') {
  Axios.defaults.baseURL = config.dev.env.API_LOCATION.split('"').join('');
  // process.env.API_LOCATION;
} else {
  Axios.defaults.baseURL = config.dev.env.API_LOCATION.split('"').join(''); // process.env.API_LOCATION;
}*/
// Axios.defaults.baseURL = process.env.API_LOCATION;

Axios.defaults.headers.common.Accept = 'application/json';
Axios.defaults.headers['Access-Control-Allow-Origin'] = '*';

// Bind Axios to Vue.
Vue.$http = Axios;
Object.defineProperty(Vue.prototype, '$http', {
  get() {
    return Axios;
  },
});
