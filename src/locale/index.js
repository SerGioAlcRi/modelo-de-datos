/* ============
 * Locale
 * ============
 *
 * For a multi-language application, you can
 * specify the languages you want to use here.
 */

import en from './en';
import nl from './nl';
import es from './es';

export default {
  en,
  nl,
  es,
};
