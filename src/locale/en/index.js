/* ============
 * English Language File
 * ============
 *
 * An example language file.
 */

import * as translate from './auth.json';

export default {
  translate,
};
