import Vue from 'vue'
import App from './App.vue'
import router from './plugins/router'
import store from './plugins/store'
import './registerServiceWorker'

import moment from 'vue-moment';
import myPluginGo from './plugins/gojs/vue-go';
import go from 'gojs';

import './plugins/axios';
import { i18n } from './plugins/vue-i18n';

import './assets/style/custom.css';
import './assets/style/bootstrap.min.css';

Vue.use(moment);

Vue.$go = go;

Vue.use(myPluginGo, {
  optionsGo: false,
});

Vue.config.productionTip = false

new Vue({
  i18n,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
