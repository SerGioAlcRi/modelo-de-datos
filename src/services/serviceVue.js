import configApp from '../config/config.service';

export default {
  wrapper(http, service, params) {
    const method = configApp[service].method;
    const config = {
      headers: {
        "Content-Type": "application/json",
        'Access-Control-Allow-Origin': '*',
        }
      }
    const headers = {};

    let exitHeaders = false;
    const parameters = params.params ? params.params : {};
    let url = configApp[service].mock ? configApp[service].mockUrl : configApp[service].url;
    const requestBody = params.params ? params.params : '';
    // Utilizado para intercambiar parametros
    if (configApp[service].baseURL) {
      http.defaults.baseURL = configApp[service].baseURL;
    }
    if (params.replace && params.arrReplace.length) {
      params.arrReplace.forEach((element) => {
        url = url.replace(`{${Object.entries(element)[0][0]}}`, Object.entries(element)[0][1]);
      });
    }
    if (params.uploadProgress) {
      config.onUploadProgress = params.onUploadProgress; /* (progressEvent) => {
        params.uploadPercentage = Math.round((progressEvent.loaded * 100) / progressEvent.total);
      }; */
    }
    if (params.Authorization) {
      headers.Authorization = params.Authorization;
      exitHeaders = true;
    }
    if (method.toUpperCase() === 'GET') {
      return http.get(url, parameters, config);
    }
    if (!exitHeaders) return http.post(url, requestBody);
    return http.post(url, requestBody, { headers });
  },
};
