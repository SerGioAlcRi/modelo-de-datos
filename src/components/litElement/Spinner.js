import { LitElement, html } from 'lit-element'

class Spinner extends LitElement {
  static get properties () {
    return { loading: { type: Boolean } }
  }

  constructor () {
    super()
    this.loading = false
  }

  render () {
    return html`
      ${this.loading ? html`
      <div class="container-spinner">
        <div class="spinner"></div>
      </div>
      ` : ''}
    `
  }
}

customElements.define('spinner', Spinner)